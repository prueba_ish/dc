var request = indexedDB.open("library");
var db;
var store;
request.onupgradeneeded = function() {
  // The database did not previously exist, so create object stores and indexes.
  console.log("onupgradeneeded");
  db = request.result;
  
  store = db.createObjectStore("books", {keyPath: "isbn"});
  var titleIndex = store.createIndex("by_title", "title", {unique: true});
  var authorIndex = store.createIndex("by_author", "author");

  // Populate with initial data.

  store.put({title: "Quarry Memories", author: "Fred", isbn: 123456});
  store.put({title: "Water Buffaloes", author: "Fred", isbn: 234567});
  store.put({title: "Bedrock Nights", author: "Barney", isbn: 345678});
  
};

//Base datos abierta con exito
request.onsuccess = function() {
  console.log("onsuccess open db");
  db = request.result;

  //transactionPut();
  //searchByTitle("Bedrock Nights");
  //getAll();
  //lookUpIndexCursor();
  pruebaControlErrores();
  
};

function transactionPut(){
  console.log("transactionPut");
  var tx = db.transaction("books", "readwrite");
  var store = tx.objectStore("books");

  store.put({title: "Quarry Memories", author: "Fred", isbn: 123456});
  store.put({title: "Water Buffaloes", author: "Fred", isbn: 234567});
  store.put({title: "Bedrock Nights", author: "Barney", isbn: 345678});

  tx.oncomplete = function() {
  // All requests have succeeded and the transaction has committed.
  };
}

function searchByTitle(paramTitle){
  console.log("searchByTitle");
  var tx = db.transaction("books", "readonly");
  var store = tx.objectStore("books");
  var index = store.index("by_title");

  var request = index.get(paramTitle);
  request.onsuccess = function() {
    console.log("get index onsuccess");
    var matching = request.result;
    if (matching !== undefined) {
      // A match was found.
      report(matching.isbn, matching.title, matching.author);
    } else {
      // No match was found.
      report(null);
    }
  };
}

function getAll(){
    var tx = db.transaction("books", "readonly");
    store = tx.objectStore("books");

    var cursorRequest = store.openCursor();
    var items;
    cursorRequest.onerror = function(error) {
        console.log(error);
    };
 
    cursorRequest.onsuccess = function(evt) { 
        console.log("Open cursor succeeded");                   
        var cursor = evt.target.result;
        if (cursor) {
            console.log(cursor.value.author);
            cursor.continue();
        }
    };
}

function lookUpIndexCursor(){
  console.log("lookUpIndexCursor");
  var tx = db.transaction("books", "readonly");
  var store = tx.objectStore("books");
  var index = store.index("by_author");

  var request = index.openCursor(IDBKeyRange.only("Fred"));
  request.onsuccess = function() {
    console.log("cursor abierto correctamente");
    var cursor = request.result;
    if (cursor) {
      // Called for each matching record.
      console.log("lookUpIndexCursor Fred");
      report(cursor.value.isbn, cursor.value.title, cursor.value.author);
      cursor.continue();
    } else {
      // No more matching records.
      console.log("No more matching records");
      //report(null);
    }
  };
}


function report(isbn, title, author){
  console.log("REPORT: Isbn:" + isbn + ", Title:" + title + ", Author:"+ author);
}


function pruebaControlErrores(){
  console.log("pruebaControlErrores");
  var tx = db.transaction("books", "readwrite");
  var store = tx.objectStore("books");
  var request = store.put({title: "Water Buffaloes", author: "Slate", isbn: 987654});

  tx.oncomplete = function() {
      console.log("oncomplete pruebaControlErrores transaction");
      cerrarTransaccion();
  };

  request.onerror = function() {
    console.log("put onerror");
  // The uniqueness constraint of the "by_title" index failed.
    report(request.error);
  // Could call request.preventDefault() to prevent the transaction from aborting.
    cerrarTransaccion();
  };
  tx.onabort = function() {
  // Otherwise the transaction will automatically abort due the failed request.
    console.log("put onabort");
    report(tx.error);
    cerrarTransaccion();
  };
}

function cerrarTransaccion(){
  console.log("Cerrar transaction");
  db.close();
}